"""
Project:        advent_of_code_python
File:           test_2015.py
Description:    Tests for the 2015 challenges.
Author(s):      Simon Cotts
"""

from typing import Dict, List

import pytest

from advent_of_code.advent_2015 import day_1, day_2, day_3, day_4, day_5, day_6


class TestDay1:
    """Unit tests for the day 1 challenges."""

    def test_part_1(self) -> None:
        """:params: None."""

        result_to_inputs: Dict[int, List[str]] = {
            0: ["(())", "()()"],
            3: ["(((", "(()(()(", "))((((("],
            -1: ["())", "))("],
            -3: [")))", ")())())"],
        }

        for result, inputs in result_to_inputs.items():
            for input in inputs:
                assert day_1(part=1, _input=input) == result

    def test_part_2(self) -> None:
        """:params: None."""

        assert day_1(part=2, _input=")") == 1
        assert day_1(part=2, _input="()())") == 5


class TestDay2:
    """Unit tests for the day 2 challenges."""

    def test_part_1(self) -> None:
        """:params: None."""

        assert 58 == day_2(part=1, _input=["2x3x4"])
        assert 43 == day_2(part=1, _input=["1x1x10"])
        assert 58 + 43 == day_2(part=1, _input=["2x3x4", "1x1x10"])

    def test_part_2(self) -> None:
        """:params: None."""

        assert day_2(part=2, _input=["2x3x4"]) == 34
        assert day_2(part=2, _input=["1x1x10"]) == 14
        assert day_2(part=2, _input=["2x3x4", "1x1x10"]) == 34 + 14


class TestDay3:
    """Unit tests for the day 3 challenges."""

    def test_part_1(self) -> None:
        """:params: None."""

        assert day_3(part=1, _input=">") == 2
        assert day_3(part=1, _input="^>v<") == 4
        assert day_3(part=1, _input="^v^v^v^v^v") == 2

    def test_part_2(self) -> None:
        """:params: None."""

        assert day_3(part=2, _input="^v") == 3
        assert day_3(part=2, _input="^>v<") == 3
        assert day_3(part=2, _input="^v^v^v^v^v") == 11


@pytest.mark.skip(reason="slow")
class TestDay4:
    """Unit tests for the day 4 challenges."""

    def test_part_1(self) -> None:
        """:params: None."""

        assert day_4(part=1, _input="abcdef") == 609043
        assert day_4(part=1, _input="pqrstuv") == 1048970

    def test_part_2(self) -> None:
        """:params: None"""

        assert day_4(part=2, _input="abcdef") == 6742839
        assert day_4(part=2, _input="pqrstuv") == 5714438


class TestDay5:
    """Unit tests for the day 5 challenges."""

    def test_part_1(self) -> None:
        """:params: None."""

        assert day_5(part=1, _input=["ugknbfddgicrmopn"]) == 1
        assert day_5(part=1, _input=["jchzalrnumimnmhp"]) == 0
        assert day_5(part=1, _input=["haegwjzuvuyypxyu"]) == 0
        assert day_5(part=1, _input=["dvszwmarrgswjxmb"]) == 0

    def test_part_2(self) -> None:
        """:params: None"""

        assert day_5(part=2, _input=["qjhvhtzxzqqjkmpb"]) == 1
        assert day_5(part=2, _input=["xxyxx"]) == 1
        assert day_5(part=2, _input=["uurcxstgmygtbstg"]) == 0
        assert day_5(part=2, _input=["ieodomkazucvgmuy"]) == 0


class TestDay6:
    """Unit tests for the day 6 challenges."""

    def test_part_1(self) -> None:
        """params: None."""

        assert day_6(part=1, _input=["turn on 0,0 through 999,999"]) == 1000000
        assert day_6(part=1, _input=["toggle 0,0 through 999,0"]) == 1000
        assert day_6(part=1, _input=["turn off 499,499 through 500,500"]) == 0
        assert day_6(part=1, _input=["turn on 499,499 through 500,500"]) == 4

    def test_part_2(self) -> None:
        """params: None."""

        assert day_6(part=2, _input=["turn on 0,0 through 0,0"]) == 1
        assert day_6(part=2, _input=["toggle 0,0 through 999,999"]) == 2000000
