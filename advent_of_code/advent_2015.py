"""
Project:        advent_of_code_python
File:           advent_2015.py
Description:    Santa was hoping for a white Christmas, but his weather machine's "snow" function is
                powered by stars, and he's fresh out! To save Christmas, he needs you to collect
                fifty stars by December 25th.

                Collect stars by helping Santa solve puzzles. Two puzzles will be made available on
                each day in the Advent calendar; the second puzzle is unlocked when you complete the
                first. Each puzzle grants one star. Good luck!
Author(s):      Simon Cotts
"""

from collections import Counter
from hashlib import md5
from typing import Dict, List, Set, Tuple


def day_1(part: int, _input: str) -> int:
    """
    --- Day 1: Not Quite Lisp ---

    :param part: Specifies which part of the challenge to get the answer for, 1 or 2.
    :param _input: The input to provide to the challenge.

    :returns: The answer for either part of the challenge
    """

    def part_1(_input: str) -> int:
        """
        Here's an easy puzzle to warm you up.

        Santa is trying to deliver presents in a large apartment building, but he can't find the
        right floor - the directions he got are a little confusing. He starts on the ground floor
        (floor 0) and then follows the instructions one character at a time.

        An opening parenthesis, (, means he should go up one floor, and a closing parenthesis, ),
        means he should go down one floor.

        The apartment building is very tall, and the basement is very deep; he will never find the
        top or bottom floors.

        For example:

            (()) and ()() both result in floor 0.
            ((( and (()(()( both result in floor 3.
            ))((((( also results in floor 3.
            ()) and ))( both result in floor -1 (the first basement level).
            ))) and )())()) both result in floor -3.

        To what floor do the instructions take Santa?

        :param _input: The sequence of braces to calculate which floor santa should go to.
        :returns: The floor the instructions take Santa to.
        """
        # Count the number of '(' (floors up) and ')' (floors down).
        floors_up: int = _input.count("(")
        floors_down: int = _input.count(")")

        return floors_up - floors_down

    def part_2(_input: str) -> int:
        """
        Now, given the same instructions, find the position of the first character that causes him
        to enter the basement (floor -1). The first character in the instructions has position 1,
        the second character has position 2, and so on.

        For example:

            ) causes him to enter the basement at character position 1.
            ()()) causes him to enter the basement at character position 5.

        What is the position of the character that causes Santa to first enter the basement?

        :param _input: The sequence of braces to calculate which floor santa should go to.
        :returns: The floor the instructions take Santa to.
        """
        # We start on floor 0, position index 0.
        floor: int = 0
        position: int = 0

        for position, instruction in enumerate(_input):
            if instruction == "(":
                floor += 1
            if instruction == ")":
                floor -= 1

            if floor == -1:
                break

        return position + 1

    if part == 1:
        return part_1(_input)
    return part_2(_input)


def day_2(part: int, _input: List[str]) -> int:
    """
    --- Day 2: I Was Told There Would Be No Math ---

    :param part: Specifies which part of the challenge to get the answer for, 1 or 2.
    :param _input: The input to provide to the challenge.

    :returns: The answer for either part of the challenge
    """

    def part_1(_input: List[str]) -> int:
        """
        The elves are running low on wrapping paper, and so they need to submit an order for more.
        They have a list of the dimensions (length l, width w, and height h) of each present, and
        only want to order exactly as much as they need.

        Fortunately, every present is a box (a perfect right rectangular prism), which makes
        calculating the required wrapping paper for each gift a little easier: find the surface area
        of the box, which is 2*l*w + 2*w*h + 2*h*l. The elves also need a little extra paper for
        each present: the area of the smallest side.

        For example:

            - A present with dimensions 2x3x4 requires 2*6 + 2*12 + 2*8 = 52 square feet of wrapping
            paper plus 6 square feet of slack, for a total of 58 square feet.
            - A present with dimensions 1x1x10 requires 2*1 + 2*10 + 2*10 = 42 square feet of
            wrapping paper plus 1 square foot of slack, for a total of 43 square feet.

        All numbers in the elves' list are in feet. How many total square feet of wrapping paper
        should they order?

        :param _input: The dimensions for all the presents as strings in the form 'lxwxh'.
        :return: The total square feet required to wrap all the presents.
        """
        all_dimensions: List[List[str]] = [dimensions.split("x") for dimensions in _input]
        total_area: int = 0

        for dimensions in all_dimensions:
            length: int = int(dimensions[0])
            width: int = int(dimensions[1])
            height: int = int(dimensions[2])
            area_lxw: int = length * width
            area_wxh: int = width * height
            area_hxl: int = height * length

            smallest_side_area: int = min((area_lxw, area_wxh, area_hxl))
            area: int = 2 * (area_lxw + area_wxh + area_hxl)
            total_area += area + smallest_side_area

        return total_area

    def part_2(_input: List[str]) -> int:
        """
        The elves are also running low on ribbon. Ribbon is all the same width, so they only have to
        worry about the length they need to order, which they would again like to be exact.

        The ribbon required to wrap a present is the shortest distance around its sides, or the
        smallest perimeter of any one face. Each present also requires a bow made out of ribbon as
        well; the feet of ribbon required for the perfect bow is equal to the cubic feet of volume
        of the present. Don't ask how they tie the bow, though; they'll never tell.

        For example:

            A present with dimensions 2x3x4 requires 2+2+3+3 = 10 feet of ribbon to wrap the present
            plus 2*3*4 = 24 feet of ribbon for the bow, for a total of 34 feet.
            A present with dimensions 1x1x10 requires 1+1+1+1 = 4 feet of ribbon to wrap the present
            plus 1*1*10 = 10 feet of ribbon for the bow, for a total of 14 feet.

        How many total feet of ribbon should they order?

        :param _input: The dimensions for all the presents as strings in the form 'lxwxh'.
        :return: The total length of ribbon required to wrap the present.
        """
        all_dimensions: List[List[str]] = [dimensions.split("x") for dimensions in _input]
        total_length: int = 0

        for dimensions in all_dimensions:
            length: int = int(dimensions[0])
            width: int = int(dimensions[1])
            height: int = int(dimensions[2])

            smallest_perimeter: int = min(
                (2 * (length + width)), (2 * (width + height)), (2 * (height + length))
            )
            volume: int = length * width * height
            total_length += smallest_perimeter + volume

        return total_length

    if part == 1:
        return part_1(_input)
    return part_2(_input)


def day_3(part: int, _input: str) -> int:
    """
    --- Day 3: Perfectly Spherical Houses in a Vacuum ---

    :param part: Specifies which part of the challenge to get the answer for, 1 or 2.
    :param _input: The input to provide to the challenge.

    :returns: The answer for either part of the challenge
    """

    def part_1(_input: str) -> int:
        """
        Santa is delivering presents to an infinite two-dimensional grid of houses.

        He begins by delivering a present to the house at his starting location, and then an elf at
        the North Pole calls him via radio and tells him where to move next. Moves are always
        exactly one house to the north (^), south (v), east (>), or west (<). After each move, he
        delivers another present to the house at his new location.

        However, the elf back at the North Pole has had a little too much eggnog, and so his
        directions are a little off, and Santa ends up visiting some houses more than once. How many
        houses receive at least one present?

        For example:

          - > delivers presents to 2 houses: one at the starting location, and one to the east.
          - ^>v< delivers presents to 4 houses in a square, including twice to the house at his
            starting/ending location.
          - ^v^v^v^v^v delivers a bunch of presents to some very lucky children at only 2 houses.

        :param _input: A string of all navigation instructions.
        :return: The number of houses that receive one or more presents.
        """
        x: int = 0  # Index of the x coordinate.
        y: int = 1  # Index of the y coordinate.
        east: Tuple[int, int] = (x, 1)  # A move east increments the x coordinate.
        west: Tuple[int, int] = (x, -1)  # A move west decrements the x coordinate.
        north: Tuple[int, int] = (y, 1)  # A move north increments the y coordinate.
        south: Tuple[int, int] = (y, -1)  # A move south decrements the y coordinate.

        # Map each direction instruction to its instruction.
        direction_map: Dict[str, Tuple[int, int]] = {">": east, "<": west, "^": north, "v": south}

        house_coordinates_list: List[List[int]] = [[0, 0]]
        for idx, direction in enumerate(_input):
            # Start off from the last coordinates.
            house_coordinates_list.append(house_coordinates_list[idx].copy())
            coordindate: int
            modifier: int
            coordindate, modifier = direction_map[direction]
            house_coordinates_list[idx + 1][coordindate] += modifier

        # Convert each coordinate pair to a tuple as sets can only be used on immutable type.
        house_coordinates_tuple: List[Tuple[int, ...]] = [
            tuple(coords) for coords in house_coordinates_list
        ]
        unique_coordinates: Set[Tuple[int, ...]] = {coords for coords in house_coordinates_tuple}
        return len(unique_coordinates)

    def part_2(_input: str) -> int:
        """
        The next year, to speed up the process, Santa creates a robot version of himself,
        Robo-Santa, to deliver presents with him.

        Santa and Robo-Santa start at the same location (delivering two presents to the same
        starting house), then take turns moving based on instructions from the elf, who is
        eggnoggedly reading from the same script as the previous year.

        This year, how many houses receive at least one present?

        For example:

          - ^v delivers presents to 3 houses, because Santa goes north, and then Robo-Santa goes
            south.
          - ^>v< now delivers presents to 3 houses, and Santa and Robo-Santa end up back where they
            started.
          - ^v^v^v^v^v now delivers presents to 11 houses, with Santa going one direction and
            Robo-Santa going the other.

        :param _input: A string of all navigation instructions.
        :return: The number of houses that receive one or more presents.
        """
        x: int = 0  # Index of the x coordinate.
        y: int = 1  # Index of the y coordinate.
        east: Tuple[int, int] = (x, 1)  # A move east increments the x coordinate.
        west: Tuple[int, int] = (x, -1)  # A move west decrements the x coordinate.
        north: Tuple[int, int] = (y, 1)  # A move north increments the y coordinate.
        south: Tuple[int, int] = (y, -1)  # A move south decrements the y coordinate.

        # Map each direction instruction to its instruction.
        direction_map: Dict[str, Tuple[int, int]] = {">": east, "<": west, "^": north, "v": south}

        santa_directions: List[str] = [_input[idx] for idx in range(0, len(_input), 2)]
        robot_directions: List[str] = [_input[idx] for idx in range(1, len(_input), 2)]
        all_house_coordinates_list: List[List[int]] = []

        for directions in (santa_directions, robot_directions):
            house_coordinates_list: List[List[int]] = [[0, 0]]

            for idx, direction in enumerate(directions):
                # Start off from the last coordinates.
                house_coordinates_list.append(house_coordinates_list[idx].copy())
                coordindate: int
                modifier: int
                coordindate, modifier = direction_map[direction]
                house_coordinates_list[idx + 1][coordindate] += modifier

            all_house_coordinates_list += house_coordinates_list

        # Convert each coordinate pair to a tuple as sets can only be used on immutable type.
        house_coordinates_tuple: List[Tuple[int, ...]] = [
            tuple(coords) for coords in all_house_coordinates_list
        ]
        unique_coordinates: Set[Tuple[int, ...]] = {coords for coords in house_coordinates_tuple}
        return len(unique_coordinates)

    if part == 1:
        return part_1(_input)
    return part_2(_input)


def day_4(part: int, _input: str) -> int:
    """
    --- Day 4: The Ideal Stocking Stuffer ---

    :param part: Specifies which part of the challenge to get the answer for, 1 or 2.
    :param _input: The input to provide to the challenge.

    :returns: The answer for either part of the challenge
    """

    def part_1(_input: str) -> int:
        """
        Santa needs help mining some AdventCoins (very similar to bitcoins) to use as gifts for all
        the economically forward-thinking little girls and boys.

        To do this, he needs to find MD5 hashes which, in hexadecimal, start with at least five
        zeroes. The input to the MD5 hash is some secret key (your puzzle input, given below)
        followed by a number in decimal.
        To mine AdventCoins, you must find Santa the lowest positive number (no leading zeroes: 1,
        2, 3, ...) that produces such a hash.

        For example:

          - If your secret key is abcdef, the answer is 609043, because the MD5 hash of abcdef609043
            starts with five zeroes (000001dbbfa...), and it is the lowest such number to do so.
          - If your secret key is pqrstuv, the lowest number it combines with to make an MD5 hash
            starting with five zeroes is 1048970; that is, the MD5 hash of pqrstuv1048970 looks like
            000006136ef....


        :param _input: The secret key.
        :return: The MD5 hash as a string.
        """
        mod: int = 0
        key: str = _input

        while not (md5(key.encode("utf-8")).hexdigest()).startswith("00000"):
            mod += 1
            key = _input + str(mod)

        return mod

    def part_2(_input: str) -> int:
        """
        Now find one that starts with six zeroes.

        :param _input: The secret key.
        :return: The MD5 hash as a string.
        """
        mod: int = 0
        key: str = _input

        while not (md5(key.encode("utf-8")).hexdigest()).startswith("000000"):
            mod += 1
            key = _input + str(mod)

        return mod

    if part == 1:
        return part_1(_input)
    return part_2(_input)


def day_5(part: int, _input: List[str]) -> int:
    """
    --- Day 5: Doesn't He Have Intern-Elves For This? ---

    :param part: Specifies which part of the challenge to get the answer for, 1 or 2.
    :param _input: The input to provide to the challenge.

    :returns: The answer for either part of the challenge
    """

    def part_1(_input: List[str]) -> int:
        """
        Santa needs help figuring out which strings in his text file are naughty or nice.

        A nice string is one with all of the following properties:

          - It contains at least three vowels (aeiou only), like aei, xazegov, or aeiouaeiouaeiou.
          - It contains at least one letter that appears twice in a row, like xx, abcdde (dd), or
          aabbccdd (aa, bb, cc, or dd).
          - It does not contain the strings ab, cd, pq, or xy, even if they are part of one of the
            other requirements.

        For example:

          - ugknbfddgicrmopn is nice because it has at least three vowels (u...i...o...), a double
            letter (...dd...), and none of the disallowed substrings.
          - aaa is nice because it has at least three vowels and a double letter, even though the
            letters used by different rules overlap.
          - jchzalrnumimnmhp is naughty because it has no double letter.
          - haegwjzuvuyypxyu is naughty because it contains the string xy.
          - dvszwmarrgswjxmb is naughty because it contains only one vowel.

        How many strings are nice?

        :param _input: The list of strings to test.
        :return: The number of nice strings.
        """
        nice_strings_count: int = 0

        for string in _input:
            # Must contain at least 3 vowels from 'aeiou'.
            counter = Counter(string)
            rule_1: bool = (
                counter["a"] + counter["e"] + counter["i"] + counter["o"] + counter["u"] > 2
            )

            # Must contain at least 1 doulble letter, e.g. 'aa', 'bb', ..., 'yy', 'zz'.
            rule_2: bool = False
            for idx, char in enumerate(string):
                if idx < len(string) - 1 and string[idx + 1] == char:
                    rule_2 = True
                    break

            # Must not contain 'ab', 'cd', 'pq', or 'xy'.
            rule_3: bool = all([False for substr in ["ab", "cd", "pq", "xy"] if substr in string])

            nice_strings_count += 1 if rule_1 and rule_2 and rule_3 else 0

        return nice_strings_count

    def part_2(_input: List[str]) -> int:
        """
        Realizing the error of his ways, Santa has switched to a better model of determining whether
        a string is naughty or nice. None of the old rules apply, as they are all clearly
        ridiculous.

        Now, a nice string is one with all the following properties:

          - It contains a pair of any two letters that appears at least twice in the string without
            overlapping, like xyxy (xy) or aabcdefgaa (aa), but not like aaa (aa, but it overlaps).
          - It contains at least one letter which repeats with exactly one letter between them, like
            xyx, abcdefeghi (efe), or even aaa.

        For example:

          - qjhvhtzxzqqjkmpb is nice because is has a pair that appears twice (qj) and a letter that
            repeats with exactly one letter between them (zxz).
          - xxyxx is nice because it has a pair that appears twice and a letter that repeats with
            one between, even though the letters used by each rule overlap.
          - uurcxstgmygtbstg is naughty because it has a pair (tg) but no repeat with a single
            letter between them.
          - ieodomkazucvgmuy is naughty because it has a repeating letter with one between (odo),
            but no pair that appears twice.

        How many strings are nice under these new rules?

        :param _input: The list of strings to test.
        :return: The number of nice strings.
        """
        nice_strings_count: int = 0

        for string in _input:
            rule_1: bool = False
            rule_2: bool = False

            for idx, char in enumerate(string):
                if idx < len(string) - 2:
                    pair: str = char + string[idx + 1]
                    if pair in string[idx + 2 :]:  # noqa: E203
                        rule_1 = True

                    if char == string[idx + 2]:
                        rule_2 = True

                    if rule_1 and rule_2:
                        nice_strings_count += 1
                        break

        return nice_strings_count

    if part == 1:
        return part_1(_input)
    return part_2(_input)


def day_6(part: int, _input: List[str]) -> int:
    """
    --- Day 6: Probably a Fire Hazard ---

    :param part: Specifies which part of the challenge to get the answer for, 1 or 2.
    :param _input: The input to provide to the challenge.

    :returns: The answer for either part of the challenge
    """

    def part_1(_input: List[str]) -> int:
        """
        Because your neighbors keep defeating you in the holiday house decorating contest year after
        year, you've decided to deploy one million lights in a 1000x1000 grid.

        Furthermore, because you've been especially nice this year, Santa has mailed you
        instructions on how to display the ideal lighting configuration.

        Lights in your grid are numbered from 0 to 999 in each direction; the lights at each corner
        are at 0,0, 0,999, 999,999, and 999,0. The instructions include whether to turn on, turn
        off, or toggle various inclusive ranges given as coordinate pairs. Each coordinate pair
        represents opposite corners of a rectangle, inclusive; a coordinate pair like 0,0 through
        2,2 therefore refers to 9 lights in a 3x3 square. The lights all start turned off.

        To defeat your neighbors this year, all you have to do is set up your lights by doing the
        instructions Santa sent you in order.

        For example:
          - turn on 0,0 through 999,999 would turn on (or leave on) every light.
          - toggle 0,0 through 999,0 would toggle the first line of 1000 lights, turning off the
            ones that were on, and turning on the ones that were off.
          - turn off 499,499 through 500,500 would turn off (or leave off) the middle four lights.

        After following the instructions, how many lights are lit?

        :param _input: The list instructions to apply.
        :return: The number lights that are lit.
        """
        grid: List[List[int]] = [[0] * 1000 for _ in range(1000)]

        for instruction in _input:
            # "toggle 461,550 through 564,900" ->
            # ["toggle", "461,550", "through" "564,900"]
            tokens: List[str] = instruction.split()
            action: str = tokens[-4]  # "toggle"
            x1: int = int(tokens[-3].split(",")[0])  # 461
            y1: int = int(tokens[-3].split(",")[1])  # 550
            x2: int = int(tokens[-1].split(",")[0])  # 564
            y2: int = int(tokens[-1].split(",")[1])  # 900

            for x in range(x1, x2 + 1):
                for y in range(y1, y2 + 1):
                    if action == "toggle":
                        grid[x][y] = 1 if grid[x][y] == 0 else 0

                    if action == "on":
                        grid[x][y] = 1

                    if action == "off":
                        grid[x][y] = 0

        lights_lit: int = sum(sum(row) for row in grid)
        return lights_lit

    def part_2(_input: List[str]) -> int:
        """
        You just finish implementing your winning light pattern when you realize you mistranslated
        Santa's message from Ancient Nordic Elvish.

        The light grid you bought actually has individual brightness controls; each light can have a
        brightness of zero or more. The lights all start at zero.

        The phrase turn on actually means that you should increase the brightness of those lights by
        1.

        The phrase turn off actually means that you should decrease the brightness of those lights
        by 1, to a minimum of zero.

        The phrase toggle actually means that you should increase the brightness of those lights by
        2.

        What is the total brightness of all lights combined after following Santa's instructions?

        For example:
          - turn on 0,0 through 0,0 would increase the total brightness by 1.
          - toggle 0,0 through 999,999 would increase the total brightness by 2000000.
        """
        grid: List[List[int]] = [[0] * 1000 for _ in range(1000)]

        for instruction in _input:
            # "toggle 461,550 through 564,900" ->
            # ["toggle", "461,550", "through" "564,900"]
            tokens: List[str] = instruction.split()
            action: str = tokens[-4]  # "toggle"
            x1: int = int(tokens[-3].split(",")[0])  # 461
            y1: int = int(tokens[-3].split(",")[1])  # 550
            x2: int = int(tokens[-1].split(",")[0])  # 564
            y2: int = int(tokens[-1].split(",")[1])  # 900

            for x in range(x1, x2 + 1):
                for y in range(y1, y2 + 1):
                    if action == "toggle":
                        grid[x][y] += 2

                    if action == "on":
                        grid[x][y] += 1

                    if action == "off":
                        grid[x][y] = 0 if grid[x][y] - 1 < 0 else grid[x][y] - 1

        lights_lit: int = sum(sum(row) for row in grid)
        return lights_lit

    if part == 1:
        return part_1(_input)
    return part_2(_input)
