"""
Project:        advent_of_code
File:           utilities.py
Description:    General utilities.
Author(s):      Simon Cotts
"""

import json
import logging
import os
import socket
import traceback
from datetime import datetime
from logging.config import dictConfig
from types import TracebackType
from typing import Any, Dict, List, Optional, Type

from ruamel.yaml import YAML

DEBUG: str = "DEBUG"


class CustomException(Exception):
    """An overarching exception type for any custom exceptions."""


def log_divider(length: int = 25, char: str = "-") -> str:
    """
    Create a string divider of a certain length.

    E.g. log_divider(10) --> "----------"
    E.g. log_divider(10, '*') --> "**********"

    :returns: The formatted string divider.
    """
    return "{:{}<{}}".format("", char, length)


def read_json_from_file(file_path: str) -> Dict:
    """
    Reads the contents of a JSON file.

    :param file_path: Path to the JSON to be read.
    :returns: The contents of the JSON file.
    """
    with open(file_path, "r") as json_file:
        json_data: Dict = json.load(json_file)

    return json_data


def write_json_to_file(json_data: Dict, file_path: str) -> None:
    """
    Dump a data structure into a JSON file.

    :param json_data: The Dict to be written to the file.
    :param file_path: The file to be written to.
    """
    with open(file_path, "w") as json_file:
        json.dump(json_data, json_file)


def read_yaml_from_file(file_path: str, sanitise: bool = False) -> Any:
    """
    Reads the contents from a YAML file using a RoundTripLoader to preserve the entire file
    structure. Removes any YAML aliases if specified.

    :param file_path: Path to the YAML file to be read.
    :param sanitise: If True, the YAML aliases are removed, if any.
                     If false, the YAML aliases are left included, if any.

    :returns: The contents of the YAML file.
    """
    with open(file_path, "r") as yaml_file:
        yaml: YAML = YAML(pure=True)
        yaml_data: Any = yaml.load(yaml_file)

    if sanitise and yaml_data:
        try:
            del yaml_data["aliases"]
        except KeyError:
            log: logging.Logger = logging.getLogger(__name__)
            log.debug("YAML Read - key 'aliases' not present.")

    return yaml_data


def write_yaml_to_file(yaml_data: Dict, file_path: str) -> None:
    """
    Dump a data structure into a YAML file using a RoundTripDumper to preserve the entire file
    structure, if present.

    :param yaml_data: The Dict to be written to the file.
    :param file_path: The file to be written to.
    """
    with open(file_path, "w") as yaml_file:
        yaml: YAML = YAML(pure=True)
        yaml.dump(yaml_data, yaml_file)


def format_exception(exc_value: Exception) -> List[str]:
    """
    Extracts the entire contents and traceback of an exception.
    Formats the exception data into a list for cleaner logging.

    :param exc_value: The exception to be formatted.
    :return: The formatted error.
    """
    exc_type: Type[Exception] = exc_value.__class__
    exc_traceback: Optional[TracebackType] = exc_value.__traceback__

    # Extract the complete filepath and line number of the offending file and line respectively.
    filepath, line_no, *_ = traceback.extract_tb(exc_traceback).pop()
    filename: str = os.path.basename(filepath)

    # Format the exception and split into lines so that it can be logged nicely.
    error: str = "".join(traceback.format_exception(exc_type, exc_value, exc_traceback))
    error_message: str = f"An exception occurred in '{filename}' on line '{line_no}'\n{error}"
    error_list: List[str] = list(filter(None, error_message.split("\n")))

    return error_list


def log_debug_exc(exc: Exception, log: logging.Logger = logging.getLogger()) -> None:
    """
    Logs the full trace of an exception into the root logger at DEBUG level.

    :param exc: The exception being logged.
    :param log: The logger to use; defaults to the root logger.
    """
    log.debug("{:*<{}}".format("", 50))
    for line in format_exception(exc):
        log.debug(line)
    log.debug("{:*<{}}".format("", 50))


class LogLevelFilter(logging.Filter):
    """A simple filter object for only isolating log messages to their specified levels."""

    def __init__(self, level: int, include_levels: Optional[List[int]] = None) -> None:
        """
        :param level: The specified logging handler level.
        :param include_levels: A list of levels to explicitly match with in addition.
        """
        self._level: int = level
        self._include_levels: List[int] = include_levels if include_levels else []

        logging.Filter.__init__(self)

    def filter(self, record: logging.LogRecord) -> bool:
        """
        Ensures that only log messages associated with the handler's exact level are recorded.
        Matches with any other levels specified in self.include_levels.

        :param record: The record to filter.
        :returns: True, if the provided log record should be filtered.
                  False, if the provided log record should not be filtered.
        """
        return record.levelno == self._level or record.levelno in self._include_levels


def create_root_logger(log_config_path: str, log_file_path: str, debug: bool = True) -> None:
    """
    Creates the root logger from a YAML config file.
    Adds new logging levels and names for custom file streams.
    Injects the current timestamp and machine name into the log file path.

    :param log_config_path: Path to a log config YAML file relative to the top-level of the
                            project. E.g. "yaml/log_config.yaml"
    :param log_file_path: Path where log files will be created.
    :param debug: Flag to determine whether DEBUG level messages will be logged.
    """
    # Explicitly define where log YAML files are so that they can be found when built as a wheel.
    project_root_dir: str = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    log_config_path = os.path.join(project_root_dir, log_config_path)

    log_config: Dict = read_yaml_from_file(log_config_path)
    timestamp: str = str(datetime.now()).replace(":", "-").replace(" ", "_")[:-7]
    machine_name: str = socket.gethostname()

    # Ensure the log file directory exists.
    if not os.path.exists(log_file_path):
        raise FileNotFoundError("Log path '%s' does not exist.", log_file_path)

    # Check if the debug file should be included.
    if not debug:
        del log_config["handlers"]["debugFile"]
        log_config["root"]["handlers"].remove("debugFile")

    for handler_name, handler_data in log_config["handlers"].items():
        # Modify the log filename of each file handler.
        if handler_data["class"] == "logging.FileHandler":
            # Add the platform specific path prefix.
            handler_data["filename"] = log_file_path + handler_data["filename"]

            # Add the relevant information onto the filename.
            handler_data["filename"] += "_{}_{}.log".format(machine_name, timestamp)

    # Add the custom log levels if present.
    if "customLevels" in log_config:
        for level_name, level_data in log_config["customLevels"].items():
            logging.addLevelName(level_data["value"], level_name)

    # Apply the modified configuration.
    dictConfig(log_config)

    # Apply the log level filter to all the file handlers.
    root_logger: logging.Logger = logging.getLogger()
    for handler in root_logger.handlers:
        level_name = log_config["handlers"][handler.name]["level"]

        if "customLevels" in log_config:
            if level_name in log_config["customLevels"]:
                include_levels: List[int] = log_config["customLevels"][level_name]["includeLevels"]
                handler.addFilter(LogLevelFilter(handler.level, include_levels))
