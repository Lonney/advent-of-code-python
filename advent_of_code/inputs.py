"""
Project:        advent_of_code_python
File:           inputs.py
Description:    Reads in the inputs for every challenge.
Author(s):      Simon Cotts
"""

from typing import Any, Dict

from advent_of_code.utilities import read_yaml_from_file

INPUTS: Dict[str, Dict[str, str]] = read_yaml_from_file("yaml/inputs.yaml")
INPUTS_2015: Dict[str, Any] = INPUTS["2015"]
