"""
Project:        advent_of_code_python
File:           main.py
Description:    Entry point of the program.
Author(s):      Simon Cotts
"""

import logging

from advent_of_code import advent_2015
from advent_of_code.inputs import INPUTS_2015
from advent_of_code.utilities import create_root_logger, log_debug_exc, log_divider


def main() -> None:
    """Entry point of the program."""

    create_root_logger(log_config_path="yaml/log_config.yaml", log_file_path=".", debug=False)
    log: logging.Logger = logging.getLogger(__name__)

    try:
        log.info("Advent of Code Python")
        log.info(log_divider())
        log.info("2015:")
        # log.info("  Day 1:")
        # log.info(f"    Part 1: floor {advent_2015.day_1(part=1, _input=INPUTS_2015['day_1'])}")
        # log.info(
        #    f"    Part 2: instruction {advent_2015.day_1(part=2, _input=INPUTS_2015['day_1'])}"
        # )
        # log.info("  Day 2:")
        # log.info(f"    Part 1: {advent_2015.day_2(part=1, _input=INPUTS_2015['day_2'])} sqft")
        # log.info(f"    Part 2: {advent_2015.day_2(part=2, _input=INPUTS_2015['day_2'])} ft")
        # log.info("  Day 3:")
        # log.info(f"    Part 1: {advent_2015.day_3(part=1, _input=INPUTS_2015['day_3'])} houses")
        # log.info(f"    Part 2: {advent_2015.day_3(part=2, _input=INPUTS_2015['day_3'])} houses")
        # log.info("  Day 4:")
        # log.info(f"    Part 1: answer {advent_2015.day_4(part=1, _input=INPUTS_2015['day_4'])}")
        # log.info(f"    Part 2: answer {advent_2015.day_4(part=2, _input=INPUTS_2015['day_4'])}")
        # log.info("  Day 5:")
        # log.info(
        #    f"    Part 1: {advent_2015.day_5(part=1, _input=INPUTS_2015['day_5'])} nice strings"
        # )
        # log.info(
        #    f"    Part 2: {advent_2015.day_5(part=2, _input=INPUTS_2015['day_5'])} nice strings"
        # )
        log.info("Day 6:")
        log.info(f"    Part 1: {advent_2015.day_6(part=1, _input=INPUTS_2015['day_6'])} lights lit")
        log.info(f"    Part 1: {advent_2015.day_6(part=2, _input=INPUTS_2015['day_6'])} brightness")

    except Exception as exc:
        log_debug_exc(exc, log)
        for exc_line in str(exc).split("\n"):
            log.error(exc_line)

    finally:
        log.info("PROGRAM END")


if __name__ == "__main__":
    main()
